### Clear-OS app-base

The base application as the name suggests is the application that launches 
by default in clear-os, this application offers the user to change some parameters of Clear-OS


### License

* http://www.gnu.org/copyleft/gpl.html


### Install it

* This application is intalled by default in clear-os


### Contributing

* www.itotafrica.com
* avan.tech

### Developers and testers

* https://github.com/itotafrica


### Translators

* All the files concerning the translation are in the lang directory


### Writers